/*
 * Copyright © 2013 – 2015 Ricki Hirner (bitfire web engineering).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package at.bitfire.dav4android.property;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;

import at.bitfire.dav4android.Constants;
import at.bitfire.dav4android.Property;
import at.bitfire.dav4android.PropertyFactory;
import at.bitfire.dav4android.XmlUtils;
import lombok.ToString;
import okhttp3.internal.http.DatesKt;

@ToString
public class GetContentLength implements Property {
    public static final Name NAME = new Name(XmlUtils.NS_WEBDAV, "getcontentlength");

    public Long contentLength;

    private GetContentLength() {}

    public GetContentLength(String rawLength)
    {
        try {
            contentLength = Long.parseLong(rawLength);
        } catch (NumberFormatException e) {
            Constants.log.log(Level.SEVERE, "Couldn't parse Content-Length: " + rawLength);
        }
    }


    public static class Factory implements PropertyFactory {
        @Override
        public Name getName() {
            return NAME;
        }

        @Override
        public GetContentLength create(XmlPullParser parser) {
            // <!ELEMENT getcontentlength (#PCDATA) >
            try {
                return new GetContentLength(parser.nextText());
            } catch(XmlPullParserException|IOException e) {
                Constants.log.log(Level.SEVERE, "Couldn't parse <getcontentlength>", e);
                return null;
            }
        }
    }
}
